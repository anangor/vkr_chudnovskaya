#include "graded_graph.h"
#include "graph_generator.h"
#include "young_graph.h"
#include "omp.h"
#include <fstream>
#include <iostream>
#include <ctime>
#include <string>

using namespace std;

int main()
{
	int num_threads = omp_get_max_threads();
	int num_levels = 10;
	Young_graph young_graph;
	Graded_graph prob_graph;
	vector<int> diag;
	map<int, double> vertice;
	bool central;
	int iter;
	ofstream status_fout("status.txt");
	ofstream result_fout ("result_parallel.txt");

	#pragma omp parallel for private(young_graph, prob_graph, diag, vertice, central, iter)
	for (int i = 0; i < num_threads; ++i){
		for (int j = num_levels+i; j < 1000; j = j+num_threads){
			central = false;
			prob_graph = young_graph.young_graph_generator(j, 1);
			iter = 0;
			while (iter < 100 and central == false){
				prob_graph.rhombus_algorithm();
				central = prob_graph.check_central_measure(0.00005);
				++iter;
			}
			#pragma omp critical
				{
				result_fout << "Graph size: " << j << endl;
				result_fout << "Is this graph central? " << central << endl;
				result_fout << "prob:\n";
				for (auto const &y : prob_graph.struct_graph.vertices[9][6]){
					result_fout << y.first << " " << y.second << endl;
				}
			}
			young_graph.young_graph_struct.diags.clear();
			prob_graph.struct_graph.vertices.clear();
			status_fout << "\n" << flush;
		}
	}



	/*int num_levels = 3;

	Young_graph young_graph;
	Graded_graph prob_graph;
	vector<int> diag;
	map<int, double> vertice;
	bool central;
	int iter;

	for(int i = num_levels; i < 20; ++i){
		central = false;
		prob_graph = young_graph.young_graph_generator(i);
		iter = 0;
		while (iter < 100 and central == false){
			prob_graph.rhombus_algorithm();
			central = prob_graph.check_central_measure(0.0000000005);
			++iter;
		}
		cout << "Graph size: " << i << endl;
		cout << "Is this graph central? " << central << endl;
		cout << "prob:\n";
		for (auto const &y : prob_graph.struct_graph.vertices[1][0]){
			cout << y.first << " " << y.second << endl;
		}

		young_graph.young_graph_struct.diags.clear();
		prob_graph.struct_graph.vertices.clear();
	}*/


	//young_graph.print_young_graph();
	/*prob_graph = prob_graph.input_graded_graph("test_graph.txt");
	prob_graph.print_graded_graph();
	for (int i = 1; i <= 5 ; ++i){
		prob_graph.rhombus_algorithm();
		prob_graph.print_graded_graph();
		prob_graph.check_central_measure(0.0000000005);
	}*/


	//bool equals;
	//vector<vector<int>> next_level = {{5},{4, 1},{3, 2},{3, 1, 1}};
	//vector<vector<int>> next_level = {{5},{3, 2}};

	//vector<int> diag = {3, 2};
	//equals = test.equal_diags(next_level, diag);
	//cout << equals << endl;
	//test.young_graph_generator();
	//Graded_graph test;
	//string filename = "planch_graph.txt";
	//test = test.input_graded_graph(filename);
	//cout << test_2.struct_graph.vertices.size() << endl;
	//test.print_graded_graph();

	/*srand((unsigned int)time(NULL));
	Generated_graph test;
	Graded_graph test_2;
	string filename = "gen_graph_2.txt";
	string filename = "test_graph_5.txt";
	//test = test.generate_graph(20, 1, 10, 1);
	test.print_graded_graph();
	test.print_graph_to_file(filename);
	test_2 = test.input_graded_graph(filename);
	test_2.print_graded_graph();*/

    return 0;
}


	/*	Graded_graph test;
	string filename = "test_graph_4.txt";

	test = test.input_graded_graph(filename);

	cout << "We start here\n";
	test.print_graded_graph();
	test.check_central_measure(0.00005);

	//test.rhombus_algorithm();
	//test.print_graded_graph();
	for (int i = 1; i <= 3 ; ++i){
		cout << "Iteration " << i << endl;
		test.rhombus_algorithm();

		test.print_graded_graph();
		test.check_central_measure(0.00005);
	}*/

	/*srand(time(NULL));

	Graded_graph test;

	set<pair<int, double>> vertice;
	vector<set<pair<int, double>>> level;
	//insert first level of edges
	vertice.insert(make_pair(0,0.5));
	vertice.insert(make_pair(1,0.5));
	level.push_back(vertice);
	test.struct_graph.vertices.push_back(level);
	//insert second level of edges
	vertice.clear();
	level.clear();
	vertice.insert(make_pair(0,0.5));
	vertice.insert(make_pair(1,0.5));
	level.push_back(vertice);
	vertice.clear();
	vertice.insert(make_pair(0,0.33));
	vertice.insert(make_pair(1,0.33));
	vertice.insert(make_pair(2,0.33));
	level.push_back(vertice);
	test.struct_graph.vertices.push_back(level);

	cout << "full graph\n";
	test.print_graded_graph();

	cout << "one element\n";
	for (auto const &x : test.struct_graph.vertices[1][1]) {
		cout << "(" << x.first << ", " << x.second << ") ";
	}*/
