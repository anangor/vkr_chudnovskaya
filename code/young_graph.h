#pragma once
#include "graded_graph.h"

class Young_graph : public Graded_graph
{
    public:
        Young_graph();
        struct Young_Graph{
    			vector<vector <vector<int>>> diags;
    		};
    		Young_Graph young_graph_struct;

        Graded_graph young_graph_generator(int num_lev, bool strict);
        int equal_diags(vector<vector<int>> next_level, vector<int> diag);
        bool is_strict(vector<int> diag);
        void print_young_graph();

    protected:

    private:
};
