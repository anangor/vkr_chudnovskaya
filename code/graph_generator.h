#pragma once
#include "graded_graph.h"

using namespace std;

class Generated_graph : public Graded_graph
{
    public:
        Generated_graph();
        void print_class();
		Generated_graph generate_graph(int levels, int min_vertices, int max_vertices, bool prob_type);

    protected:

    private:
};
