#include "graph_generator.h"
#include <iostream>


using namespace std;

Generated_graph::Generated_graph(){};


void Generated_graph::print_class(){
	cout << "My class is generated graph\n";
}

Generated_graph Generated_graph::generate_graph(int levels, int min_vertices, int max_vertices, bool prob_type){
	Generated_graph graph;
	map<int, double> vertice;
	vector<map<int, double>> level;
	int num_vertices;
	double prob;
	for (int i = 1; i <= levels; i++){
		for (int j = 0; j < i; j++){
				num_vertices = min_vertices + rand()%(max_vertices-min_vertices+1);
				for (int k = 0; k < num_vertices; k++){
					if (prob_type == 0){
						vertice.insert(make_pair(k, double(1)/num_vertices));
					}
					else if (prob_type == 1){
						prob = (rand()%1000)/(1000 * 1.0);
						vertice.insert(make_pair(k, prob));
					}
				}
			level.push_back(vertice);
			vertice.clear();
		}
		graph.struct_graph.vertices.push_back(level);
		level.clear();
	}
	graph.normalize_weights();
	return graph;
}
