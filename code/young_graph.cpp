#include "young_graph.h"
#include <iostream>

using namespace std;

Young_graph::Young_graph(){};

Graded_graph Young_graph::young_graph_generator(int num_lev, bool strict){
    Young_graph young_graph;
    Graded_graph prob_graph;
    vector <int> temp_diag;
    vector <vector<int>> level = {{1}};
    vector <vector<int>> next_level;
    map<int, double> prob_vertice;
    vector<map<int, double>> prob_level;
    double path_prob;
    int equals;
    bool add_diag;
    if (!strict){
      add_diag = 1;
    }

    young_graph_struct.diags.push_back(level);

    for (int num = 0; num < num_lev; ++num){
      for (int i = 0; i < level.size(); ++i){
        path_prob = 0;
        temp_diag = level.at(i);

        temp_diag.at(0) = temp_diag.at(0)+1;
        ++path_prob;

        equals = equal_diags(next_level, temp_diag);
        if (!equals){
          next_level.push_back(temp_diag);

          prob_vertice.insert(make_pair(next_level.size()-1, 0));
        }
        else{
          prob_vertice.insert(make_pair(equals, 0));
        }

        for (int k = 1; k < temp_diag.size(); ++k){
          temp_diag = level.at(i);
          if (temp_diag.at(k-1) > temp_diag.at(k)){
            temp_diag.at(k) = temp_diag.at(k) + 1;
            if (strict){
              add_diag = is_strict(temp_diag);
            }
            if (add_diag){
              ++path_prob;
              equals = equal_diags(next_level, temp_diag);
              if (!equals){
                next_level.push_back(temp_diag);

                prob_vertice.insert(make_pair(next_level.size()-1, 0));
              }
              else{
                prob_vertice.insert(make_pair(equals, 0));
              }
            }
          }
        }

        temp_diag = level.at(i);
        temp_diag.push_back(1);
        if (strict){
          add_diag = is_strict(temp_diag);
        }
        if (add_diag){
          ++path_prob;
          equals = equal_diags(next_level, temp_diag);
          if (!equals){
            next_level.push_back(temp_diag);

            prob_vertice.insert(make_pair(next_level.size()-1, 0));
          }
          else{
            prob_vertice.insert(make_pair(equals, 0));
          }
        }

        path_prob = 1/path_prob;
        for (auto &x : prob_vertice){
          x.second = path_prob;
        }

        prob_level.push_back(prob_vertice);
        prob_vertice.clear();
      }

      prob_graph.struct_graph.vertices.push_back(prob_level);
      young_graph_struct.diags.push_back(next_level);
      level = next_level;
      next_level = {};
      prob_level = {};
    }
    return prob_graph;
      //prob_graph.print_graded_graph();
}

bool Young_graph::is_strict(vector<int> diag){
  for (int i = 1; i < diag.size(); ++i){
    if (diag.at(i-1) < diag.at(i)+1){
      return false;
    }
  }
  return true;
}

int Young_graph::equal_diags(vector<vector<int>> next_level, vector<int> diag){
  vector <int> temp_diag;
  bool equals;
  int eq_diag;
  for (int i = 0; i < next_level.size(); ++i){
    temp_diag = next_level.at(i);
    if (temp_diag.size() == diag.size()){
        equals = true;
      for (int j = 0; j < temp_diag.size(); ++j){
        if (temp_diag.at(j) != diag.at(j)){
          equals = false;
        }
      }
      if (equals){
        return i;
      }
    }
  }
  return 0;
}


void Young_graph::print_young_graph(){
  for (int i = 0; i < young_graph_struct.diags.size(); ++i){
    for (int j = 0; j < young_graph_struct.diags.at(i).size(); ++j){
      for (int k = 0; k < young_graph_struct.diags.at(i).at(j).size(); ++k){
          cout << young_graph_struct.diags.at(i).at(j).at(k) << " ";
      }
      cout << "; ";
    }
    cout << endl;
  }
}
