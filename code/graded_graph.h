#pragma once
#include <vector>
#include <set>
#include <utility>
#include <string>
#include <map>

using namespace std;

class Graded_graph
{
public:
		struct Graph{
			vector<vector<map<int, double>>> vertices;
		};
		Graph struct_graph;

		void print_class_parent();

		void update_path_weight(int line, int elem, int dest, double weight);

    void print_graded_graph();
		void print_graph_to_file(string filename);
		Graded_graph input_graded_graph(string filename);
		bool check_central_measure(double eps);

		void rhombus_algorithm();
		map<int, pair<int, set <int>>> return_number_of_paths(int line, int elem);
		vector<pair<int,double>> rhombus_weights(bool update, int line, int elem_1, int elem_2, int elem_3, int elem_4);
		vector<pair<int,double>> update_weights(vector<pair<int,double>> weights);
		void normalize_weights();

    protected:


    private:
};
