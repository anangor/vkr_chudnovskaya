#include "graded_graph.h"
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <iomanip>
#include <math.h>

using namespace std;

void Graded_graph::update_path_weight(int line, int elem, int dest, double weight){
	map<int,double>::iterator itr;
	itr = struct_graph.vertices[line][elem].find(dest);
	if (itr != struct_graph.vertices[line][elem].end() && itr->second != weight){
		itr->second = weight;
	}
}

void Graded_graph::print_graded_graph(){
	for (int i = 0; i < struct_graph.vertices.size(); i++) {
		for (int j = 0; j < struct_graph.vertices[i].size(); j++){
			for (auto const &x : struct_graph.vertices[i][j]) {
				cout << "(" << x.first << ", " << setprecision(3) << x.second << ") ";
			}
			cout << "; ";
		}
		cout << endl;
    }
}

void Graded_graph::print_graph_to_file(string filename){
	ofstream fout(filename);
	for (int i = 0; i < struct_graph.vertices.size(); i++) {
		for (int j = 0; j < struct_graph.vertices[i].size(); j++){
			for (auto const &x : struct_graph.vertices[i][j]) {
				fout << x.first << " " << setprecision(3) << x.second << ",";

			}
			fout << "; ";
		}
		fout << endl;
    }
}

Graded_graph Graded_graph::input_graded_graph(string filename){
	ifstream file(filename);
	Graded_graph graph;
	if (file.is_open()) {
		map<int, double> vertice;
		vector<map<int, double>> level;
		string line;
		size_t line_pos = 0;
		size_t subline_pos = 0;
		size_t element_pos = 0;
		string subline;
		string element;
		string element_id;
		string element_weight;
		string line_delimiter = ";";
		string subline_delimiter = ",";
		string element_delimiter = " ";
		string first_elem;
		bool line_end;
		bool subline_end;
		while (getline(file, line)) {
			line = line.substr(0, line.size()-2);
			line_end = false;
			//cout << line << endl;
			while (!line_end) {
				line_pos = line.find(line_delimiter);
				if (line_pos == string::npos){
					line_end = true;
					subline = line;
				}
				else {
					subline = line.substr(0, line_pos);
					line.erase(0, line_pos + line_delimiter.length());
				}
				subline = subline.substr(0, subline.size()-1);
				first_elem = subline[0];
				if (first_elem == " "){
					subline = subline.substr(1, subline.size());
				}
				//cout << subline << endl;
				subline_end = false;
				while (!subline_end) {
					subline_pos = subline.find(subline_delimiter);
					if (subline_pos == string::npos){
						subline_end = true;
						element = subline;
					}
					else {
						element = subline.substr(0, subline_pos);
						subline.erase(0, subline_pos + subline_delimiter.length());
					}
					//cout << element << endl;
					element_pos = element.find(element_delimiter);
					element_id = element.substr(0, element_pos);
					element.erase(0, element_pos + element_delimiter.length());
					element_weight = element;
					//cout << element_id << endl;
					//cout << element_weight << endl;
					vertice.insert(make_pair(stoi(element_id), stod(element_weight)));

				}
				level.push_back(vertice);
				vertice.clear();
			}

			graph.struct_graph.vertices.push_back(level);
			level.clear();
		}
	}
	file.close();
	return graph;
}

map<int, pair<int, set <int>>> Graded_graph::return_number_of_paths(int line, int elem){
	map<int, pair<int, set <int>>> number_of_paths;
	for (auto const &x : struct_graph.vertices[line][elem]) {
		for (auto const &y : struct_graph.vertices[line+1][x.first]) {

			if (number_of_paths.count(y.first)){
				number_of_paths[y.first].first++;
				number_of_paths[y.first].second.insert(x.first);
			}
			else{
				number_of_paths[y.first].first = 1;
				number_of_paths[y.first].second.insert(x.first);
			}
		}

	}
	return number_of_paths;
}


vector<pair<int,double>> Graded_graph::rhombus_weights(bool update, int line, int elem_1, int elem_2, int elem_3, int elem_4){
	vector<pair<int,double>> weights(4);
	vector<pair<int,double>> new_weights(4);

	for (auto const &x : struct_graph.vertices[line][elem_1]) {
		if (x.first == elem_2){
			weights[0] = make_pair(elem_2, x.second);
		}
		else if (x.first == elem_3){
			weights[1] = make_pair(elem_3, x.second);
		}
	}
	for (auto const &x : struct_graph.vertices[line+1][elem_2]){
		if (x.first == elem_4){
			weights[2] = make_pair(elem_4, x.second);
		}
	}

	for (auto const &x : struct_graph.vertices[line+1][elem_3]){
		if (x.first == elem_4){
			weights[3] = make_pair(elem_4, x.second);
		}
	}

	if (!update){
		return weights;
	}
	else{
		new_weights = update_weights(weights);
		return new_weights;
	}
}

vector<pair<int,double>> Graded_graph::update_weights(vector<pair<int,double>> weights){
	if (weights[0].second*weights[2].second != weights[1].second*weights[3].second){
		weights[1].second = weights[0].second*weights[2].second/weights[3].second;
	}
	return weights;
}

void Graded_graph::normalize_weights(){
	double sum;
	for (int line = 0; line < struct_graph.vertices.size()-1; ++line){
		for (int elem = 0; elem < struct_graph.vertices[line].size(); ++elem){
			sum = 0;
			for (auto const &x : struct_graph.vertices[line][elem]){
				sum+=x.second;
			}
			if (sum != 1) {
				for (auto const &x : struct_graph.vertices[line][elem]){
					update_path_weight(line, elem, x.first, x.second/sum);
				}
			}
		}
	}
}


void Graded_graph::rhombus_algorithm(){
	map<int, pair<int, set <int>>> number_of_paths;
	vector<pair<int,double>> weights;
	set <int> checked_paths;
	for(int line = 0; line < struct_graph.vertices.size()-1; ++line){
		for(int elem = 0; elem < struct_graph.vertices[line].size(); ++elem){

			number_of_paths = return_number_of_paths(line, elem);

			for(auto it = number_of_paths.begin(); it != number_of_paths.end(); ++it){
				if (it->second.first > 1){
					for (auto i = it->second.second.begin(); i != it->second.second.end(); ++i){
						for (auto j = next(i); j != it->second.second.end(); ++j){
							if (checked_paths.find(*j) == checked_paths.end()){
								//cout <<  *j <<" : this element wasn't changed\n";
								checked_paths.insert(*j);
							}

							weights = rhombus_weights(true, line, elem, *i, *j, it->first);
							update_path_weight(line, elem, weights[1].first, weights[1].second);
						}
					}
				}
			}

		}
	}
	normalize_weights();
}

bool Graded_graph::check_central_measure(double eps){
	map<int, pair<int, set <int>>> number_of_paths;
	vector<pair<int,double>> weights;
	double abs_diff = 0;
	for (int line = 0; line < struct_graph.vertices.size()-1; ++line){
		for (int elem = 0; elem < struct_graph.vertices[line].size(); ++elem){
			number_of_paths = return_number_of_paths(line, elem);

			for(auto it = number_of_paths.begin(); it != number_of_paths.end(); ++it){
				if (it->second.first > 1){
					for (auto i = it->second.second.begin(); i != it->second.second.end(); ++i){
						for (auto j = next(i); j != it->second.second.end(); ++j){
							weights = rhombus_weights(false, line, elem, *i, *j, it->first);
							//cout << "weights: ";
							//cout << weights[0].second <<" " << weights[2].second <<" "<< weights[1].second<<" "<<weights[3].second << endl;
							abs_diff = abs(weights[0].second*weights[2].second - weights[1].second*weights[3].second);
							//cout << "abs_diff = " << abs_diff << endl;
							if (abs_diff>=eps){
								return false;
							}

						}
					}
				}
			}
		}
	}
	return true;
}
